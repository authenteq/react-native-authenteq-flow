const path = require('path');

module.exports = {
  dependency: {
    platforms: {
      ios: { podspecPath: path.join(__dirname, "react-native-authenteq-flow.podspec") }
    }
  }
};