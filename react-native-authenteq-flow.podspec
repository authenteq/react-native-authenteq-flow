require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "react-native-authenteq-flow"
  s.version      = package["version"]
  s.summary      = package["description"]
  s.description  = <<-DESC
                   React Native module for Authenteq Flow
                   DESC
  s.homepage     = "https://bitbucket.org/authenteq/react-native-authenteq-flow.git"
  s.license      = "commercial"
  s.authors      = { "Authenteq" => "info@authenteq.com" }
  s.platforms    = { :ios => "11.0" }
  s.source       = { :git => "https://bitbucket.org/authenteq/react-native-authenteq-flow.git" }

  s.swift_version = "5"
  s.source_files = "ios/**/*.{h,m,swift}"
  s.requires_arc = true

  s.dependency "React"
  s.dependency "AuthenteqFlow", "1.76.0"
  s.info_plist = {
    'NSCameraUsageDescription' => 'We need access to your camera to proceed with identification'
  }
end
