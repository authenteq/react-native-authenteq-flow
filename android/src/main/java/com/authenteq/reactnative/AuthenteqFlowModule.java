package com.authenteq.reactnative;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.util.SparseArray;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.authenteq.android.flow.result.DocumentIdentificationResult;
import com.authenteq.android.flow.result.DriversLicenseIdentificationResult;
import com.authenteq.android.flow.result.IdCardIdentificationResult;
import com.authenteq.android.flow.result.IdentificationResult;
import com.authenteq.android.flow.result.PassportIdentificationResult;
import com.authenteq.android.flow.ui.faceauthentication.FaceAuthenticationActivity;
import com.authenteq.android.flow.ui.faceauthentication.FaceAuthenticationParams;
import com.authenteq.android.flow.ui.faceauthentication.FaceAuthenticationParamsWithClientSecret;
import com.authenteq.android.flow.ui.faceauthentication.FaceAuthenticationParamsWithToken;
import com.authenteq.android.flow.ui.identification.IdentificationActivity;
import com.authenteq.android.flow.ui.identification.IdentificationParams;
import com.authenteq.android.flow.ui.identification.IdentificationParamsWithClientSecret;
import com.authenteq.android.flow.ui.identification.IdentificationParamsWithToken;
import com.authenteq.android.flow.ui.theme.AuthenteqTheme;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableType;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;

import org.threeten.bp.LocalDate;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuthenteqFlowModule extends ReactContextBaseJavaModule {

    private static final int REQUEST_CODE_IDENTIFICATION = 1001;
    private static final int REQUEST_CODE_FACE_AUTHENTICATION = 1002;

    private final SparseArray<Promise> promises;

    private String styleName;

    private final BaseActivityEventListener activityEventListener = new BaseActivityEventListener() {
        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
            switch (requestCode) {
                case REQUEST_CODE_IDENTIFICATION: {
                    final Promise promise = promises.get(REQUEST_CODE_IDENTIFICATION);
                    if (resultCode == Activity.RESULT_OK) {
                        final WritableMap map = new WritableNativeMap();
                        IdentificationResult result = IdentificationActivity.getResult(data);
                        if (result != null) {
                            map.putString("verificationId", result.getVerificationId());
                            map.putString("selfieImageFilePath", result.getSelfieImageFilePath());
                            map.putString("proofOfAddressFilePath", result.getProofOfAddressFilePath());
                            map.putArray("documents", createDocuments(result.getDocuments()));

                            // deprecated
                            map.putString("selfieImageFile", result.getSelfieImageFilePath());
                        }
                        promise.resolve(map);
                    } else {
                        final Throwable error = IdentificationActivity.getError(data);
                        if (error != null) {
                            promise.reject(error);
                        } else {
                            promise.reject("userCanceled", "User canceled flow");
                        }
                    }
                }
                break;
                case REQUEST_CODE_FACE_AUTHENTICATION: {
                    final Promise promise = promises.get(REQUEST_CODE_FACE_AUTHENTICATION);
                    if (resultCode == Activity.RESULT_OK) {
                        final WritableMap map = new WritableNativeMap();
                        final String code = FaceAuthenticationActivity.getResult(data);
                        map.putString("code", code);
                        promise.resolve(map);
                    } else {
                        final Throwable error = FaceAuthenticationActivity.getError(data);
                        if (error != null) {
                            promise.reject(error);
                        } else {
                            promise.reject("userCanceled", "User canceled flow");
                        }
                    }
                }
                break;
            }
        }
    };

    private WritableArray createDocuments(List<DocumentIdentificationResult> documents) {
        final WritableArray array = new WritableNativeArray();
        for (DocumentIdentificationResult document : documents) {
            if (document instanceof PassportIdentificationResult) {
                array.pushMap(createPassport(((PassportIdentificationResult) document)));
            } else if (document instanceof IdCardIdentificationResult) {
                array.pushMap(createIdCard(((IdCardIdentificationResult) document)));
            } else if (document instanceof DriversLicenseIdentificationResult) {
                array.pushMap(createDriversLicense(((DriversLicenseIdentificationResult) document)));
            }
        }
        return array;
    }

    private WritableMap createDriversLicense(DriversLicenseIdentificationResult document) {
        final WritableMap map = new WritableNativeMap();
        map.putString("givenNames", document.getGivenNames());
        map.putString("surname", document.getSurname());
        map.putString("surnameAndGivenNames", document.getSurnameAndGivenNames());
        final LocalDate dateOfBirth = document.getDateOfBirth();
        map.putString("dateOfBirth", dateOfBirth == null ? null : dateOfBirth.toString());
        map.putString("sex", document.getSex() == null ? null : document.getSex().toString());
        final LocalDate dateOfExpiry = document.getDateOfExpiry();
        map.putString("dateOfExpiry", dateOfExpiry == null ? null : dateOfExpiry.toString());
        final LocalDate dateOfIssue = document.getDateOfIssue();
        map.putString("dateOfIssue", dateOfIssue == null ? null : dateOfIssue.toString());
        map.putString("issuingCountry", document.getIssuingCountry());
        map.putString("documentNumber", document.getDocumentNumber());
        map.putString("documentType", document.getDocumentType().toString());
        map.putString("address", document.getAddress());
        map.putString("placeOfBirth", document.getPlaceOfBirth());
        map.putString("issuingAuthority", document.getIssuingAuthority());
        map.putString("personalNumber", document.getPersonalNumber());
        map.putString("givenNamesLocalized", document.getGivenNamesLocalized());
        map.putString("surnameAndGivenNamesLocalized", document.getSurnameAndGivenNamesLocalized());
        map.putString("placeOfBirthLocalized", document.getPlaceOfBirthLocalized());
        map.putString("issuingAuthorityLocalized", document.getIssuingAuthorityLocalized());
        map.putString("addressLocalized", document.getAddressLocalized());
        map.putString("jurisdiction", document.getJurisdiction());
        map.putString("licenseClass", document.getLicenseClass());
        final Map<String, DriversLicenseIdentificationResult.LicenseClassDetails> licenseClassDetails = document.getLicenseClassDetails();
        final WritableMap licenseClassDetailsMap = new WritableNativeMap();
        for (Map.Entry<String, DriversLicenseIdentificationResult.LicenseClassDetails> entry : licenseClassDetails.entrySet()) {
            final WritableMap entryMap = new WritableNativeMap();
            final DriversLicenseIdentificationResult.LicenseClassDetails classDetails = entry.getValue();
            final LocalDate from = classDetails.getFrom();
            if (from != null) entryMap.putString("from", from.toString());
            final LocalDate to = classDetails.getTo();
            if (to != null) entryMap.putString("to", to.toString());
            final String notes = classDetails.getNotes();
            if (notes != null) entryMap.putString("notes", notes);
            licenseClassDetailsMap.putMap(entry.getKey(), entryMap);
        }
        map.putMap("licenseClassDetails", licenseClassDetailsMap);
        putBoolean(map, "isSixteenPlus", document.isSixteenPlus());
        putBoolean(map, "isEighteenPlus", document.isEighteenPlus());
        putBoolean(map, "isTwentyOnePlus", document.isTwentyOnePlus());
        map.putString("documentFrontImageFilePath", document.getDocumentFrontImageFilePath());
        map.putString("documentBackImageFilePath", document.getDocumentBackImageFilePath());

        // deprecated fields
        map.putString("documentFrontFile", document.getDocumentFrontImageFilePath());
        map.putString("documentBackFile", document.getDocumentBackImageFilePath());
        return map;
    }

    private WritableMap createIdCard(IdCardIdentificationResult document) {
        final WritableMap map = new WritableNativeMap();
        map.putString("givenNames", document.getGivenNames());
        map.putString("surname", document.getSurname());
        map.putString("surnameAndGivenNames", document.getSurnameAndGivenNames());
        final LocalDate dateOfBirth = document.getDateOfBirth();
        map.putString("dateOfBirth", dateOfBirth == null ? null : dateOfBirth.toString());
        final LocalDate dateOfExpiry = document.getDateOfExpiry();
        map.putString("dateOfExpiry", dateOfExpiry == null ? null : dateOfExpiry.toString());
        final LocalDate dateOfIssue = document.getDateOfIssue();
        map.putString("dateOfIssue", dateOfIssue == null ? null : dateOfIssue.toString());
        map.putString("sex", document.getSex() == null ? null : document.getSex().toString());
        map.putString("nationality", document.getNationality());
        map.putString("issuingCountry", document.getIssuingCountry());
        map.putString("documentNumber", document.getDocumentNumber());
        map.putString("documentType", document.getDocumentType().toString());
        map.putString("address", document.getAddress());
        map.putString("placeOfBirth", document.getPlaceOfBirth());
        map.putString("issuingAuthority", document.getIssuingAuthority());
        map.putString("personalNumber", document.getPersonalNumber());
        map.putString("surnameLocalized", document.getPersonalNumber());
        map.putString("givenNamesLocalized", document.getGivenNamesLocalized());
        map.putString("surnameAndGivenNamesLocalized", document.getSurnameAndGivenNamesLocalized());
        map.putString("placeOfBirthLocalized", document.getPlaceOfBirthLocalized());
        map.putString("issuingAuthorityLocalized", document.getIssuingAuthorityLocalized());
        map.putString("addressLocalized", document.getAddressLocalized());
        putBoolean(map, "isSixteenPlus", document.isSixteenPlus());
        putBoolean(map, "isEighteenPlus", document.isEighteenPlus());
        putBoolean(map, "isTwentyOnePlus", document.isTwentyOnePlus());
        map.putString("documentFrontImageFilePath", document.getDocumentFrontImageFilePath());
        map.putString("documentBackImageFilePath", document.getDocumentBackImageFilePath());

        // deprecated fields
        map.putString("documentFrontFile", document.getDocumentFrontImageFilePath());
        map.putString("documentBackFile", document.getDocumentBackImageFilePath());
        return map;
    }

    private WritableMap createPassport(PassportIdentificationResult document) {
        final WritableMap map = new WritableNativeMap();
        map.putString("givenNames", document.getGivenNames());
        map.putString("surname", document.getSurname());
        map.putString("surnameAndGivenNames", document.getSurnameAndGivenNames());
        final LocalDate dateOfBirth = document.getDateOfBirth();
        map.putString("dateOfBirth", dateOfBirth == null ? null : dateOfBirth.toString());
        final LocalDate dateOfExpiry = document.getDateOfExpiry();
        map.putString("dateOfExpiry", dateOfExpiry == null ? null : dateOfExpiry.toString());
        final LocalDate dateOfIssue = document.getDateOfIssue();
        map.putString("dateOfIssue", dateOfIssue == null ? null : dateOfIssue.toString());
        map.putString("sex", document.getSex() == null ? null : document.getSex().toString());
        map.putString("nationality", document.getNationality());
        map.putString("issuingCountry", document.getIssuingCountry());
        map.putString("documentNumber", document.getDocumentNumber());
        map.putString("documentType", document.getDocumentType().toString());
        map.putString("address", document.getAddress());
        map.putString("placeOfBirth", document.getPlaceOfBirth());
        map.putString("issuingAuthority", document.getIssuingAuthority());
        map.putString("personalNumber", document.getPersonalNumber());
        map.putString("givenNamesLocalized", document.getGivenNamesLocalized());
        map.putString("surnameAndGivenNamesLocalized", document.getSurnameAndGivenNamesLocalized());
        map.putString("placeOfBirthLocalized", document.getPlaceOfBirthLocalized());
        map.putString("issuingAuthorityLocalized", document.getIssuingAuthorityLocalized());
        map.putString("addressLocalized", document.getAddressLocalized());
        putBoolean(map, "isSixteenPlus", document.isSixteenPlus());
        putBoolean(map, "isEighteenPlus", document.isEighteenPlus());
        putBoolean(map, "isTwentyOnePlus", document.isTwentyOnePlus());
        map.putString("documentFrontImageFilePath", document.getDocumentImageFilePath());

        // deprecated
        map.putString("documentFrontFile", document.getDocumentImageFilePath());

        return map;
    }

    public AuthenteqFlowModule(ReactApplicationContext reactContext) {
        super(reactContext);
        reactContext.addActivityEventListener(activityEventListener);
        promises = new SparseArray<>();
    }

    private void putBoolean(@NonNull WritableMap map, @NonNull String key, @Nullable Boolean value) {
        if (value == null) {
            map.putNull(key);
        } else {
            map.putBoolean(key, value);
        }
    }

    private int getStyle() {
        return getStyle(styleName);
    }

    private int getStyle(String styleName) {
        if (styleName == null) {
            return R.style.AuthenteqCustom;
        }
        try {
            Field resourceField = R.style.class.getDeclaredField(styleName);
            return resourceField.getInt(resourceField);
        } catch (Exception e) {
            return R.style.AuthenteqCustom;
        }
    }

    private AuthenteqTheme getDynamicTheme(ReadableMap theme) {
        AuthenteqTheme dynamicTheme = new AuthenteqTheme();

        // Colors
        dynamicTheme.setPrimaryColor(parseColor(readMapString(theme, ThemeKey.PRIMARY_COLOR)));
        dynamicTheme.setTextColor(parseColor(readMapString(theme, ThemeKey.TEXT_COLOR)));
        dynamicTheme.setScreenBackgroundColor(parseColor(readMapString(theme, ThemeKey.SCREEN_BACKGROUND_COLOR)));
        dynamicTheme.setSelectedButtonTextColor(parseColor(readMapString(theme, ThemeKey.SELECTED_BUTTON_TEXT_COLOR)));
        dynamicTheme.setSelectedButtonBackgroundColor(parseColor(readMapString(theme, ThemeKey.SELECTED_BUTTON_BACKGROUND_COLOR)));
        dynamicTheme.setViewBackgroundHighlightColor(parseColor(readMapString(theme, ThemeKey.VIEW_BACKGROUND_HIGHLIGHT_COLOR)));
        dynamicTheme.setSeparatorColor(parseColor(readMapString(theme, ThemeKey.SEPARATOR_COLOR)));

        // Button Corner
        dynamicTheme.setButtonCornerRadius(readMapFloat(theme, ThemeKey.BUTTON_CORNER_RADIUS.value));

        // Fonts
        dynamicTheme.setFont(readMapString(theme, ThemeKey.FONT));
        dynamicTheme.setBoldFont(readMapString(theme, ThemeKey.BOLD_FONT));

        // Illustrations
        dynamicTheme.setIdentificationInstructionImageForSelfie(readMapString(theme, ThemeKey.IDENTIFICATION_INSTRUCTION_IMAGE_FOR_SELFIE));
        dynamicTheme.setIdentificationInstructionImageForDriverLicense(readMapString(theme, ThemeKey.IDENTIFICATION_INSTRUCTION_IMAGE_FOR_DRIVER_LICENSE));
        dynamicTheme.setIdentificationInstructionImageForIdCard(readMapString(theme, ThemeKey.IDENTIFICATION_INSTRUCTION_IMAGE_FOR_ID_CARD));
        dynamicTheme.setIdentificationInstructionImageForPassport(readMapString(theme, ThemeKey.IDENTIFICATION_INSTRUCTION_IMAGE_FOR_PASSPORT));
        dynamicTheme.setIdentificationNfcSymbolCheck(readMapString(theme, ThemeKey.IDENTIFICATION_NFC_SYMBOL_CHECK));
        dynamicTheme.setIdentificationNfcInstruction(readMapString(theme, ThemeKey.IDENTIFICATION_NFC_INSTRUCTION));
        dynamicTheme.setIdentificationNfcScanInside(readMapString(theme, ThemeKey.IDENTIFICATION_NFC_SCAN_INSIDE));
        dynamicTheme.setIdentificationInstructionImageForProofOfAddress(readMapString(theme, ThemeKey.IDENTIFICATION_INSTRUCTION_IMAGE_FOR_PROOF_OF_ADDRESS));

        return dynamicTheme;
    }

    private Integer parseColor(String hexColor) {
        if (hexColor == null) {
            return null;
        }
        try {
            return Color.parseColor(hexColor);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    private Float readMapFloat(ReadableMap map, String key) {
        if (!map.hasKey(key)) {
            return null;
        }
        ReadableType type = map.getType(key);
        if (type == ReadableType.Number) {
            return (float) map.getDouble(key);
        }
        if (type == ReadableType.String) {
            String value = map.getString(key);
            if (value == null) {
                return null;
            }
            try {
                return (float) Double.parseDouble(value);
            } catch (RuntimeException ignored) {
            }
        }
        return null;
    }

    @Nullable
    private String readMapString(ReadableMap map, String key) {
        return map.hasKey(key) ? map.getString(key) : null;
    }

    @Nullable
    private String readMapString(ReadableMap map, ParamKey key) {
        return readMapString(map, key.value);
    }

    @Nullable
    private String readMapString(ReadableMap map, ThemeKey key) {
        return readMapString(map, key.value);
    }

    // Public Methods

    @NonNull
    @Override
    public String getName() {
        return "RNAuthenteqFlow";
    }

    @ReactMethod
    public void applyCustomTheme(ReadableMap values) {
        styleName = readMapString(values, ParamKey.ANDROID_STYLE);
    }

    @ReactMethod
    public void identification(String clientId, String clientSecret, ReadableArray steps, Promise promise) {
        final Activity activity = getReactApplicationContext().getCurrentActivity();
        try {
            IdentificationParams params = new IdentificationParamsWithClientSecret(
                    clientId,
                    clientSecret,
                    getStyle()
            );
            IdentificationActivity.startForResult(
                    activity,
                    REQUEST_CODE_IDENTIFICATION,
                    params
            );
            promises.put(REQUEST_CODE_IDENTIFICATION, promise);
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void identificationWithParameters(ReadableMap parameters, Promise promise) {
        final Activity activity = getReactApplicationContext().getCurrentActivity();
        String clientId = parameters.getString(ParamKey.CLIENT_ID.value);
        String clientSecret = readMapString(parameters, ParamKey.CLIENT_SECRET);
        String flowId = readMapString(parameters, ParamKey.FLOW_ID);
        String token = readMapString(parameters, ParamKey.TOKEN);
        String url = readMapString(parameters, ParamKey.URL);
        String language = readMapString(parameters, ParamKey.LANGUAGE);
        int themeResource = getStyle(null);
        AuthenteqTheme dynamicTheme = null;
        if (parameters.hasKey(ParamKey.THEME.value)) {
            ReadableMap theme = parameters.getMap(ParamKey.THEME.value);
            if (theme.hasKey(ParamKey.ANDROID_STYLE.value)) {
                themeResource = getStyle(theme.getString(ParamKey.ANDROID_STYLE.value));
            } else {
                dynamicTheme = getDynamicTheme(theme);
            }
        }
        try {
            IdentificationParams params;
            if (token != null) {
                params = new IdentificationParamsWithToken(
                        clientId,
                        token,
                        themeResource
                );
            } else {
                params = new IdentificationParamsWithClientSecret(
                        clientId,
                        clientSecret,
                        themeResource
                );
                params.setFlowId(flowId);
            }
            if (url != null) {
                params.setServerEndpoint(url);
            }
            if (language != null) {
                params.setExtra(new HashMap<String, String>() {{
                    put(SDK_EXTRA_KEY_LANGUAGE, language);
                }});
            }
            if (dynamicTheme != null) {
                params.setDynamicTheme(dynamicTheme);
            }
            IdentificationActivity.startForResult(
                    activity,
                    REQUEST_CODE_IDENTIFICATION,
                    params
            );
            promises.put(REQUEST_CODE_IDENTIFICATION, promise);
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void faceAuthenticationWithParameters(ReadableMap parameters, Promise promise) {
        final Activity activity = getReactApplicationContext().getCurrentActivity();
        String verificationId = parameters.getString(ParamKey.VERIFICATION_ID.value);
        String clientId = parameters.getString(ParamKey.CLIENT_ID.value);
        String clientSecret = readMapString(parameters, ParamKey.CLIENT_SECRET);
        String token = readMapString(parameters, ParamKey.TOKEN);
        String url = readMapString(parameters, ParamKey.URL);
        String language = readMapString(parameters, ParamKey.LANGUAGE);
        int themeResource = getStyle(null);
        AuthenteqTheme dynamicTheme = null;
        if (parameters.hasKey(ParamKey.THEME.value)) {
            ReadableMap theme = parameters.getMap(ParamKey.THEME.value);
            if (theme.hasKey(ParamKey.ANDROID_STYLE.value)) {
                themeResource = getStyle(theme.getString(ParamKey.ANDROID_STYLE.value));
            } else {
                dynamicTheme = getDynamicTheme(theme);
            }
        }
        try {
            FaceAuthenticationParams params;
            if (token != null) {
                params = new FaceAuthenticationParamsWithToken(
                        clientId,
                        token,
                        verificationId,
                        themeResource
                );
            } else {
                params = new FaceAuthenticationParamsWithClientSecret(
                        clientId,
                        clientSecret,
                        verificationId,
                        themeResource
                );
            }
            if (url != null) {
                params.setServerEndpoint(url);
            }
            if (language != null) {
                params.setExtra(new HashMap<String, String>() {{
                    put(SDK_EXTRA_KEY_LANGUAGE, language);
                }});
            }
            if (dynamicTheme != null) {
                params.setDynamicTheme(dynamicTheme);
            }
            FaceAuthenticationActivity.startForResult(
                    activity,
                    REQUEST_CODE_FACE_AUTHENTICATION,
                    params
            );
            promises.put(REQUEST_CODE_FACE_AUTHENTICATION, promise);
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void getVersion(Callback callback) {
        callback.invoke(com.authenteq.android.flow.BuildConfig.VERSION_NAME);
    }

    enum ParamKey
    {
        CLIENT_ID("clientId"),
        CLIENT_SECRET("clientSecret"),
        FLOW_ID("flowId"),
        TOKEN("token"),
        URL("url"),
        LANGUAGE("language"),
        THEME("theme"),
        ANDROID_STYLE("AndroidStyle"),
        VERIFICATION_ID("verificationId");

        private final String value;

        ParamKey(String value) {
            this.value = value;
        }
    }

    enum ThemeKey
    {
        PRIMARY_COLOR("primaryColor"),
        TEXT_COLOR("textColor"),
        SCREEN_BACKGROUND_COLOR("screenBackgroundColor"),
        SELECTED_BUTTON_TEXT_COLOR("selectedButtonTextColor"),
        SELECTED_BUTTON_BACKGROUND_COLOR("selectedButtonBackgroundColor"),
        VIEW_BACKGROUND_HIGHLIGHT_COLOR("viewBackgroundHighlightColor"),
        SEPARATOR_COLOR("separatorColor"),

        BUTTON_CORNER_RADIUS("buttonCornerRadius"),

        FONT("font"),
        BOLD_FONT("boldFont"),

        IDENTIFICATION_INSTRUCTION_IMAGE_FOR_SELFIE("identificationInstructionImageForSelfie"),
        IDENTIFICATION_INSTRUCTION_IMAGE_FOR_DRIVER_LICENSE("identificationInstructionImageForDriverLicense"),
        IDENTIFICATION_INSTRUCTION_IMAGE_FOR_ID_CARD("identificationInstructionImageForIdCard"),
        IDENTIFICATION_INSTRUCTION_IMAGE_FOR_PASSPORT("identificationInstructionImageForPassport"),
        IDENTIFICATION_NFC_SYMBOL_CHECK("identificationNfcSymbolCheck"),
        IDENTIFICATION_NFC_INSTRUCTION("identificationNfcInstruction"),
        IDENTIFICATION_NFC_SCAN_INSIDE("identificationNfcScanInside"),
        IDENTIFICATION_INSTRUCTION_IMAGE_FOR_PROOF_OF_ADDRESS("identificationInstructionImageForProofOfAddress");

        private final String value;

        ThemeKey(String value) {
            this.value = value;
        }
    }

    private static final String SDK_EXTRA_KEY_LANGUAGE = "LANGUAGE";
}
