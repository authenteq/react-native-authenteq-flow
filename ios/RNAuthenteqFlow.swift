//
//  RNAuthenteqFlow.swift
//
//  Copyright (C) 2013-2023 by FNZ. All rights reserved.
//

import Foundation
import React
import AuthenteqFlow

@objc(RNAuthenteqFlow)
class RNAuthenteqFlow: NSObject {

  // MARK: - Vars
  
  private var resolverBlock: RCTPromiseResolveBlock?
  private var rejecterBlock: RCTPromiseRejectBlock?
  private var authenteqViewController: UIViewController?
  
  // MARK: - Input Parameters
  
  private enum ParamKey: String {
    case verificationId
    case clientId
    case clientSecret
    case flowId
    case token
    case url
    case language
    case theme
    case androidStyle = "AndroidStyle"
  }
  
  // MARK: - Public Methods
  
  @objc func identification(
    _ clientId: String,
    clientSecret: String,
    documents: [[String]],
    resolver resolve: @escaping RCTPromiseResolveBlock,
    rejecter reject: @escaping RCTPromiseRejectBlock
    ) {
    resolverBlock = resolve
    rejecterBlock = reject
    DispatchQueue.main.async {
      let params = IdentificationParams(
        clientId: clientId,
        clientSecret: clientSecret
      )
      let viewController = AuthenteqFlow.instance.identificationViewController(
        with: params,
        delegate: self
      )
      viewController.modalPresentationStyle = .fullScreen
      viewController.modalTransitionStyle = .crossDissolve
      let rootViewController = UIApplication.shared.keyWindow?.rootViewController
      rootViewController?.present(viewController, animated: true)
      self.authenteqViewController = viewController
    }
  }
  
  @objc func identificationWithParameters(
    _ parameters: [String: Any],
    resolver resolve: @escaping RCTPromiseResolveBlock,
    rejecter reject: @escaping RCTPromiseRejectBlock
  ) {
    let clientSecret = readMapString(parameters, key: .clientSecret)
    let flowId = readMapString(parameters, key: .flowId)
    let token = readMapString(parameters, key: .token)
    let url = readMapString(parameters, key: .url)
    let language = readMapString(parameters, key: .language)
    guard
      let clientId = readMapString(parameters, key: .clientId),
      (clientSecret != nil || token != nil)
    else {
      reject(
        errorCode(from: AuthenteqFlowError.invalidConfiguration),
        errorDescription(from: AuthenteqFlowError.invalidConfiguration),
        nsError(from: AuthenteqFlowError.invalidConfiguration)
      )
      return
    }
    resolverBlock = resolve
    rejecterBlock = reject
    
    DispatchQueue.main.async {
      if let theme = parameters[ParamKey.theme.rawValue] as? [String:Any] {
        applyCustomTheme(theme)
      }
      let params: IdentificationParams
      if let clientSecret = clientSecret {
        params = IdentificationParams(
          clientId: clientId,
          clientSecret: clientSecret
        )
        params.flowId = flowId
      } else {
        params = IdentificationParams(
          clientId: clientId,
          token: token ?? ""
        )
      }
      if let url = url {
        params.serverEndpoint = url
      }
      if let language = language {
        params.extra = [
          RNAuthenteqFlow.sdkExtraKeyLanguage: language
        ]
      }
      let viewController = AuthenteqFlow.instance.identificationViewController(
        with: params,
        delegate: self
      )
      viewController.modalPresentationStyle = .fullScreen
      viewController.modalTransitionStyle = .crossDissolve
      
      let rootViewController = UIApplication.shared.keyWindow?.rootViewController
      rootViewController?.present(viewController, animated: true)
      
      self.authenteqViewController = viewController
    }
  }
  
  @objc func faceAuthenticationWithParameters(
    _ parameters: [String: Any],
    resolver resolve: @escaping RCTPromiseResolveBlock,
    rejecter reject: @escaping RCTPromiseRejectBlock
  ) {
    let clientSecret = readMapString(parameters, key: .clientSecret)
    let token = readMapString(parameters, key: .token)
    let url = readMapString(parameters, key: .url)
    let language = readMapString(parameters, key: .language)
    guard
      let verificationId = readMapString(parameters, key: .verificationId),
      let clientId = readMapString(parameters, key: .clientId),
      (clientSecret != nil || token != nil)
    else {
      reject(
        errorCode(from: AuthenteqFlowError.invalidConfiguration),
        errorDescription(from: AuthenteqFlowError.invalidConfiguration),
        nsError(from: AuthenteqFlowError.invalidConfiguration)
      )
      return
    }
    resolverBlock = resolve
    rejecterBlock = reject
    
    DispatchQueue.main.async {
      if let theme = parameters[ParamKey.theme.rawValue] as? [String:Any] {
        applyCustomTheme(theme)
      }
      let params: FaceAuthenticationParams
      if let clientSecret = clientSecret {
        params = FaceAuthenticationParams(
          clientId: clientId,
          clientSecret: clientSecret,
          verificationId: verificationId
        )
      } else {
        params = FaceAuthenticationParams(
          clientId: clientId,
          token: token ?? "",
          verificationId: verificationId
        )
      }
      if let url = url {
        params.serverEndpoint = url
      }
      if let language = language {
        params.extra = [
          RNAuthenteqFlow.sdkExtraKeyLanguage: language
        ]
      }
      let viewController = AuthenteqFlow.instance.faceAuthenticationViewController(
        with: params,
        delegate: self
      )
      viewController.modalPresentationStyle = .fullScreen
      UIApplication.shared.keyWindow?.rootViewController?.present(viewController, animated: true)
      self.authenteqViewController = viewController
    }
  }

  @objc func getVersion(_ callback: RCTResponseSenderBlock) {
    let version = Bundle(for: AuthenteqFlow.self).infoDictionary?["CFBundleShortVersionString"] as? String ?? "?"
    callback([version])
  }

  // MARK: - Private Methods
  
  private static let sdkExtraKeyLanguage = "LANGUAGE"
  
  private func readMapString(_ map: [String: Any], key: ParamKey) -> String? {
    map[key.rawValue] as? String
  }
  
  private func toDictionary(result: IdentificationResult) -> [String: Any?] {
    var dictionary = [String: Any]()
    dictionary["verificationId"] = result.verificationId
    dictionary["selfieImageFilePath"] = result.selfieImageFilePath
    dictionary["proofOfAddressFilePath"] = result.proofOfAddressFilePath
    dictionary["documents"] = result.documents.map({ toDictionary(document: $0) })
    
    // deprecated
    dictionary["selfieImageFile"] = result.selfieImageFilePath
    
    return dictionary
  }
  
  private func toDictionary(document: DocumentIdentificationResult) -> [String: Any?] {
    switch document {
    case let document as PassportIdentificationResult:
      return toDictionary(passport: document)
    case let document as IdCardIdentificationResult:
      return toDictionary(idCard: document)
    case let document as DriversLicenseIdentificationResult:
      return toDictionary(driversLicense: document)
    default:
      return [:]
    }
  }
  
  private func toDictionary(passport document: PassportIdentificationResult) -> [String: Any?] {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    var dictionary = [String: Any]()
    dictionary["documentFrontFilePath"] = document.documentImageFilePath
    dictionary["documentNumber"] = document.documentNumber
    dictionary["documentType"] = document.documentType
    dictionary["givenNames"] = document.givenNames
    dictionary["surname"] = document.surname
    dictionary["surnameAndGivenNames"] = document.surnameAndGivenNames
    dictionary["isEighteenPlus"] = document.isEighteenPlus?.boolValue
    dictionary["isSixteenPlus"] = document.isSixteenPlus?.boolValue
    dictionary["isTwentyOnePlus"] = document.isTwentyOnePlus?.boolValue
    dictionary["issuingCountry"] = document.issuingCountry
    dictionary["nationality"] = document.nationality
    dictionary["sex"] = document.sex
    dictionary["dateOfBirth"] = dateFormatter.string(fromOptional: document.dateOfBirth)
    dictionary["dateOfExpiry"] = dateFormatter.string(fromOptional: document.dateOfExpiry)
    dictionary["dateOfIssue"] = dateFormatter.string(fromOptional: document.dateOfIssue)
    dictionary["address"] = document.address
    dictionary["placeOfBirth"] = document.placeOfBirth
    dictionary["issuingAuthority"] = document.issuingAuthority
    dictionary["personalNumber"] = document.personalNumber
    dictionary["givenNamesLocalized"] = document.givenNamesLocalized
    dictionary["surnameAndGivenNamesLocalized"] = document.surnameAndGivenNamesLocalized
    dictionary["placeOfBirthLocalized"] = document.placeOfBirthLocalized
    dictionary["issuingAuthorityLocalized"] = document.issuingAuthorityLocalized
    dictionary["addressLocalized"] = document.addressLocalized

    // deprecated fields
    dictionary["documentFrontFile"] = document.documentImageFilePath
    return dictionary
  }
  
  private func toDictionary(idCard document: IdCardIdentificationResult) -> [String: Any?] {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    var dictionary = [String: Any]()
    dictionary["documentBackFilePath"] = document.documentBackImageFilePath
    dictionary["documentFrontFilePath"] = document.documentFrontImageFilePath
    dictionary["documentNumber"] = document.documentNumber
    dictionary["documentType"] = document.documentType
    dictionary["givenNames"] = document.givenNames
    dictionary["surname"] = document.surname
    dictionary["surnameAndGivenNames"] = document.surnameAndGivenNames
    dictionary["isEighteenPlus"] = document.isEighteenPlus?.boolValue
    dictionary["isSixteenPlus"] = document.isSixteenPlus?.boolValue
    dictionary["isTwentyOnePlus"] = document.isTwentyOnePlus?.boolValue
    dictionary["issuingCountry"] = document.issuingCountry
    dictionary["nationality"] = document.nationality
    dictionary["sex"] = document.sex
    dictionary["dateOfBirth"] = dateFormatter.string(fromOptional: document.dateOfBirth)
    dictionary["dateOfExpiry"] = dateFormatter.string(fromOptional: document.dateOfExpiry)
    dictionary["dateOfIssue"] = dateFormatter.string(fromOptional: document.dateOfIssue)
    dictionary["address"] = document.address
    dictionary["placeOfBirth"] = document.placeOfBirth
    dictionary["issuingAuthority"] = document.issuingAuthority
    dictionary["personalNumber"] = document.personalNumber
    dictionary["givenNamesLocalized"] = document.givenNamesLocalized
    dictionary["surnameAndGivenNamesLocalized"] = document.surnameAndGivenNamesLocalized
    dictionary["placeOfBirthLocalized"] = document.placeOfBirthLocalized
    dictionary["issuingAuthorityLocalized"] = document.issuingAuthorityLocalized
    dictionary["addressLocalized"] = document.addressLocalized

    // deprecated fields
    dictionary["documentBackFile"] = document.documentBackImageFilePath
    dictionary["documentFrontFile"] = document.documentFrontImageFilePath
    return dictionary
  }

  private func toDictionary(driversLicense document: DriversLicenseIdentificationResult) -> [String: Any?] {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    var dictionary = [String: Any]()
    dictionary["documentBackFilePath"] = document.documentBackImageFilePath
    dictionary["documentFrontFilePath"] = document.documentFrontImageFilePath
    dictionary["documentNumber"] = document.documentNumber
    dictionary["documentType"] = document.documentType
    dictionary["givenNames"] = document.givenNames
    dictionary["surname"] = document.surname
    dictionary["surnameAndGivenNames"] = document.surnameAndGivenNames
    dictionary["isEighteenPlus"] = document.isEighteenPlus?.boolValue
    dictionary["isSixteenPlus"] = document.isSixteenPlus?.boolValue
    dictionary["isTwentyOnePlus"] = document.isTwentyOnePlus?.boolValue
    dictionary["issuingCountry"] = document.issuingCountry
    dictionary["sex"] = document.sex
    dictionary["dateOfBirth"] = dateFormatter.string(fromOptional: document.dateOfBirth)
    dictionary["dateOfExpiry"] = dateFormatter.string(fromOptional: document.dateOfExpiry)
    dictionary["dateOfIssue"] = dateFormatter.string(fromOptional: document.dateOfIssue)
    dictionary["address"] = document.address
    dictionary["placeOfBirth"] = document.placeOfBirth
    dictionary["issuingAuthority"] = document.issuingAuthority
    dictionary["personalNumber"] = document.personalNumber
    dictionary["jurisdiction"] = document.jurisdiction
    dictionary["licenseClass"] = document.licenseClass
    let licenseClassDetail: [String: Any]? = document.licenseClassDetails?.mapValues({ (detail) in
      var dictionaryDetail = [String: Any]()
      dictionaryDetail["from"] = dateFormatter.string(fromOptional: detail.from)
      dictionaryDetail["to"] = dateFormatter.string(fromOptional: detail.to)
      dictionaryDetail["notes"] = detail.notes
      return dictionaryDetail
    })
    dictionary["licenseClassDetails"] = licenseClassDetail
    dictionary["givenNamesLocalized"] = document.givenNamesLocalized
    dictionary["surnameAndGivenNamesLocalized"] = document.surnameAndGivenNamesLocalized
    dictionary["placeOfBirthLocalized"] = document.placeOfBirthLocalized
    dictionary["issuingAuthorityLocalized"] = document.issuingAuthorityLocalized
    dictionary["addressLocalized"] = document.addressLocalized

    // deprecated fields
    dictionary["documentBackFile"] = document.documentBackImageFilePath
    dictionary["documentFrontFile"] = document.documentFrontImageFilePath
    return dictionary
  }

  private func errorDescription(from error: AuthenteqFlowError) -> String {
    switch error {
    case .authenteqLicenseError:
      return "License error"
    case .cancelledByUser:
      return "User cancelled flow"
    case .ocrSetupError:
      return "Setup error"
    case .jailbreakDetected:
      return "Device jailbreak detected"
    case .internalSetupError:
      return "Setup error"
    case .livenessSetupError:
      return "Setup error"
    case .connectionCertificateError:
      return "Unsecure internet connection"
    case .invalidConfiguration:
      return "Configuration not allowed"
    case .invalidAuthToken:
      return "Invalid Authention Token"
    case .requiredAgeNotMet:
      return "Required age not met"
    case .invalidFlowID:
      return "Invalid Flow ID"
    @unknown default:
      return "Unknown Error"
    }
  }

  private func errorCode(from error: AuthenteqFlowError) -> String {
    switch error {
    case .authenteqLicenseError:
      return "authenteqLicenseError"
    case .cancelledByUser:
      return "userCancelled"
    case .ocrSetupError:
      return "ocrSetupError"
    case .jailbreakDetected:
      return "jailbreakDetected"
    case .internalSetupError:
      return "internalSetupError"
    case .livenessSetupError:
      return "livenessSetupError"
    case .connectionCertificateError:
      return "connectionCertificateError"
    case .invalidConfiguration:
      return "invalidConfiguration"
    case .invalidAuthToken:
      return "invalidAuthToken"
    case .requiredAgeNotMet:
      return "requiredAgeNotMet"
    case .invalidFlowID:
      return "invalidFlowID"
    @unknown default:
      return "unknownError"
    }
  }

  private func nsError(from error: AuthenteqFlowError) -> NSError {
    return NSError(
      domain: "com.authenteq",
      code: error.rawValue,
      userInfo: [NSLocalizedDescriptionKey: errorDescription(from: error)]
    )
  }

  private func closeAuthenteqFlow() {
    authenteqViewController?.dismiss(animated: true)
    authenteqViewController = nil
    resolverBlock = nil
    rejecterBlock = nil
  }
}

// MARK: - Theme Keys

fileprivate enum ThemeKey: String {
  // Colors
  case primaryColor
  case textColor
  case screenBackgroundColor
  case selectedButtonTextColor
  case selectedButtonBackgroundColor
  case viewBackgroundHighlightColor
  case separatorColor

  // Buttons
  case buttonCornerRadius
  
  // Fonts
  case font
  case boldFont
  
  // Images
  case identificationInstructionImageForSelfie
  case identificationInstructionImageForDriverLicense
  case identificationInstructionImageForIdCard
  case identificationInstructionImageForPassport
  case identificationNfcSymbolCheck
  case identificationNfcInstruction
  case identificationNfcScanInside
  case identificationInstructionImageForProofOfAddress
}

fileprivate func applyCustomTheme(_ values: [String: Any]) {
  let theme = AuthenteqFlow.instance.theme
  
  // Colors
  if let primaryColor = UIColor(hexString: stringValue(values, key: .primaryColor) ?? "") {
    theme.primaryColor = primaryColor
  }
  if let textColor = UIColor(hexString: stringValue(values, key: .textColor) ?? "") {
    theme.textColor = textColor
  }
  if let screenBackgroundColor = UIColor(hexString: stringValue(values, key: .screenBackgroundColor) ?? "") {
    theme.screenBackgroundColor = screenBackgroundColor
  }
  if let viewBackgroundHighlightColor = UIColor(hexString: stringValue(values, key: .viewBackgroundHighlightColor) ?? "") {
    theme.viewBackgroundHightlightColor = viewBackgroundHighlightColor
  }
  if let separatorColor = UIColor(hexString: stringValue(values, key: .separatorColor) ?? "") {
    theme.separatorColor = separatorColor
  }
  if let selectedButtonTextColor = UIColor(hexString: stringValue(values, key: .selectedButtonTextColor) ?? "") {
    theme.selectedButtonTextColor = selectedButtonTextColor
  }
  if let selectedButtonBackgroundColor = UIColor(hexString: stringValue(values, key: .selectedButtonBackgroundColor) ?? "") {
    theme.selectedButtonBackgroundColor = selectedButtonBackgroundColor
  }
  
  // Buttons
  if let value = values[ThemeKey.buttonCornerRadius.rawValue] as? Float {
    theme.buttonCornerRadius = CGFloat(value)
  }
  
  // Fonts
  if let font = UIFont(name: stringValue(values, key: .font) ?? "", size: 16) {
    theme.font = font
  }
  if let boldFont = UIFont(name:stringValue(values, key: .boldFont) ?? "", size: 16) {
    theme.boldFont = boldFont
  }
  
  // Images
  theme.identificationInstructionImageForSelfie = stringValue(values, key: .identificationInstructionImageForSelfie)
  theme.identificationInstructionImageForPassport = stringValue(values, key: .identificationInstructionImageForPassport)
  theme.identificationInstructionImageForDriverLicense = stringValue(values, key: .identificationInstructionImageForDriverLicense)
  theme.identificationInstructionImageForIdCard = stringValue(values, key: .identificationInstructionImageForIdCard)
  theme.identificationNfcSymbolCheck = stringValue(values, key: .identificationNfcSymbolCheck)
  theme.identificationNfcInstruction = stringValue(values, key: .identificationNfcInstruction)
  theme.identificationNfcScanInside = stringValue(values, key: .identificationNfcScanInside)
  theme.identificationInstructionImageForProofOfAddress = stringValue(values, key: .identificationInstructionImageForProofOfAddress)
}

fileprivate func stringValue(_ values: [String: Any], key: ThemeKey) -> String? {
  return values[key.rawValue] as? String
}

// MARK: - AuthenteqIdentificationDelegate

extension RNAuthenteqFlow: AuthenteqIdentificationDelegate {

  func authenteqDidFinishIdentification(with result: IdentificationResult) {
    let dictionary = toDictionary(result: result)
    resolverBlock?(dictionary)
    closeAuthenteqFlow()
  }

  func authenteqDidFailIdentification(with error: AuthenteqFlowError) {
    rejecterBlock?(errorCode(from: error), errorDescription(from: error), nsError(from: error))
    closeAuthenteqFlow()
  }
}

extension RNAuthenteqFlow: AuthenteqFaceAuthenticationDelegate {
  
  func authenteqDidFinishFaceAuthentication(with code: String) {
    var dictionary = [String: Any]()
    dictionary["code"] = code
    resolverBlock?(dictionary)
    closeAuthenteqFlow()
  }
  
  func authenteqDidFailFaceAuthentication(with error: AuthenteqFlowError) {
    rejecterBlock?(errorCode(from: error), errorDescription(from: error), nsError(from: error))
    closeAuthenteqFlow()
  }
}

// MARK: - UIColor extension

fileprivate extension UIColor {
  
  convenience init?(hexString: String) {
    var chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
    switch chars.count {
    case 3: chars = chars.flatMap { [$0, $0] }; fallthrough
    case 6: chars = ["F","F"] + chars
    case 8: break
    default: return nil
    }
    self.init(red: .init(strtoul(String(chars[2...3]), nil, 16)) / 255,
              green: .init(strtoul(String(chars[4...5]), nil, 16)) / 255,
              blue: .init(strtoul(String(chars[6...7]), nil, 16)) / 255,
              alpha: .init(strtoul(String(chars[0...1]), nil, 16)) / 255)
  }
  
  convenience init?(hex: Int, transparency: CGFloat = 1) {
    let red = CGFloat((hex >> 16) & 0xFF)
    let green = CGFloat((hex >> 8) & 0xFF)
    let blue = CGFloat(hex & 0xFF)
    self.init(
      red: red / 255.0,
      green: green / 255.0,
      blue: blue / 255.0,
      alpha: min(max(transparency, 0), 1)
    )
  }
}

// MARK: - DateFormatter extension

fileprivate extension DateFormatter {

  func string(fromOptional date: Date?) -> String? {
    if let date = date {
      return string(from: date as Date)
    } else {
      return nil
    }
  }
}

