//
//  RNAuthenteqFlow.m
//
//  Copyright (C) 2013-2021 by Authenteq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(RNAuthenteqFlow, NSObject)

RCT_EXTERN_METHOD(identification:(NSString *) clientId
                  clientSecret:(NSString *) clientSecret
                  documents:(NSArray *) documents
                  resolver:(RCTPromiseResolveBlock) resolve
                  rejecter:(RCTPromiseRejectBlock) reject)

RCT_EXTERN_METHOD(identificationWithParameters:(NSDictionary *) parameters
                  resolver:(RCTPromiseResolveBlock) resolve
                  rejecter:(RCTPromiseRejectBlock) reject)

RCT_EXTERN_METHOD(faceAuthenticationWithParameters:(NSDictionary *) parameters
                  resolver:(RCTPromiseResolveBlock) resolve
                  rejecter:(RCTPromiseRejectBlock) reject)

RCT_EXTERN_METHOD(getVersion:(RCTResponseSenderBlock)callback)

+ (BOOL)requiresMainQueueSetup
{
  return YES;
}

@end
